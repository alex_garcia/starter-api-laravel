<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Permission;
use Illuminate\Console\Command;

class PermissionRefresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Permissions refresh';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        collect(config('permissions'))->each(function ($item) {
            if (! Permission::find($item['name'])) {
                try {
                    \DB::beginTransaction();
                        $permission = new Permission;
                        $permission->name = $item['name'];
                        $permission->visible = isset($item['visible']) ? $item['visible'] : true;
                        $permission->save();
                        $permission->roles()->attach($item['roles']);

                        collect($item['roles'])->each(function($role) use ($permission) {
                            User::all()->filter(function($query) use ($role) {
                                return $query->role->name == $role;
                            })->each(function($user) use ($permission) {
                                $user = User::find($user->id);
                                $user->permissions()->attach($permission->name);
                            });
                        });
                    \DB::commit();
                    $this->line('<fg=green;>' . __('general.app.Console.Commands.PermissionRefresh.done') . ': ' . '<fg=white>' . $item['name'] . '</>');
                } catch (\Exception $exception) {
                    \DB::rollBack();
                    $this->line('<fg=red;>' . __('general.app.Console.Commands.PermissionRefresh.error') . ': ' . '<fg=white>' . $item['name'] . '</>');
                }
            }
        });

        $this->line('<fg=green;>' . __('general.app.Console.Commands.PermissionRefresh.success') . '</>');
    }
}
