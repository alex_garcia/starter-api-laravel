<?php

namespace App\Traits;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

trait ApiResponser
{
	private function response($data, $code)
	{
		return response()->json($data, $code);
	}

	protected function objectResponse(Array $data, $code = 200, $status = 'success')
	{
		return $this->response(['status' => $status, 'code' => $code, 'data' => $data], $code);
	}

	protected function messageResponse($message, $code = 200, $status = 'success')
	{
		return $this->response(['status' => $status, 'code' => $code, 'message' => $message], $code);
	}

	protected function responseJsonResource(JsonResource $data, $code = 200, $status = 'success')
	{
		return $this->response(['status' => $status, 'code' => $code, 'data' => $data], $code);
    }
        
    protected function responseResourceCollection(ResourceCollection $collection, $code = 200, $status = 'success')
	{
		$paginated = $collection->resource->toArray();
        $meta = $this->meta($paginated);
        $meta = collect($meta)->filter(function ($value, $key) {
            return $key !== 'links';
        });
        
		$response = [
			'status' => $status,
			'code' => $code,
			'data' => $collection,
			'meta' => $meta
		];
		
		return $this->response($response, $code);
	}

	/**
     * Get the pagination links for the response.
     *
     * @param  array  $paginated
     * @return array
     */
    private function paginationLinks($paginated)
    {
        return [
            'first' => $paginated['first_page_url'] ?? null,
            'last' => $paginated['last_page_url'] ?? null,
            'prev' => $paginated['prev_page_url'] ?? null,
            'next' => $paginated['next_page_url'] ?? null,
        ];
    }

	/**
     * Gather the meta data for the response.
     *
     * @param  array  $paginated
     * @return array
     */
    private function meta($paginated)
    {
        return Arr::except($paginated, [
            'data',
            'first_page_url',
            'last_page_url',
            'prev_page_url',
            'next_page_url',
        ]);
    }
}