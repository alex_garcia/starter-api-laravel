<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * Set the primary key tag.
     *
     * @var string
     */
    protected $primaryKey = 'name';

    /**
     * Indicates if the primary key is increaseable.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'visible' => true
    ];

    /**
     * Relationship Belongs To Many Permission
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * Relationship Has Many User
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::updating(function ($model) {
            throw new \Exception('Can not update the permission.');
        });

        self::deleting(function ($model) {
            throw new \Exception('Can not delete the permission.');
        });
    }
}
