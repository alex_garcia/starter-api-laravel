<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->registerPolicies();

        collect(config('permissions'))->each(function ($permission) {
            Gate::define($permission['name'], function ($user) use ($permission) {
                if ($user->role->name === 'owner' || $user->role->name === 'developer') {
                    return true;
                } else {
                    $userPermisions = $user->permissions;
                    if ($userPermisions->contains($permission['name'])) {
                        if (isset($permission['required_permissions'])) {
                            $requirePermissions = collect($permission['required_permissions']);
                                $havePermision = ! $requirePermissions->map(function ($requirePermission) use ($userPermisions) {
                                    return $userPermisions->contains($requirePermission);
                                })->contains(false);
                            return $havePermision;
                        } else {
                            return true;
                        }
                    } else {
                        return false;
                    }
                }
            });
        });

    }
}
