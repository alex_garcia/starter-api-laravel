<?php

namespace App\Http\Resources\Backoffice\Administration\User\UserController;

use Illuminate\Http\Resources\Json\ResourceCollection;

class IndexCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($model) {
            return [
                'id' => $model->id,
                'full_name' => $model->full_name,
                'username' => $model->username,
                'email' => $model->email,
                'phone' => $model->phone,
                'profile_image' => $model->profile_image,
                'role_name' => $model->role_name,
                'status' => $model->status
            ];
        });
    }
}
