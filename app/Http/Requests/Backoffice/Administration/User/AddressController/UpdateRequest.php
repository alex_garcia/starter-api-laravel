<?php

namespace App\Http\Requests\Backoffice\Administration\User\AddressController;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'street' => 'nullable|string',
            'neighborhood' => 'nullable|string',
            'outdoor_number' => ['nullable', 'string', 'regex:/^(SN|[0-9]{1,4})$/'],
            'interior_number' => 'nullable|string',
            'zip_code' => 'nullable|digits:5',
            'country' => 'nullable|string',
            'state' => 'nullable|string',
            'city' => 'nullable|string',
            'between' => 'nullable|string',
            'reference' => 'nullable|string',
            'latitude' => 'nullable|string',
            'longitude' => 'nullable|string',
        ];
    }
}
