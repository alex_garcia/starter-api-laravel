<?php

namespace App\Http\Requests\Backoffice\Administration\User\AddressController;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'street' => 'required|string',
            'neighborhood' => 'required|string',
            'outdoor_number' => ['required', 'string', 'regex:/^(SN|[0-9]{1,4})$/'],
            'interior_number' => 'nullable|string',
            'zip_code' => 'required|digits:5',
            'country' => 'required|string',
            'state' => 'required|string',
            'city' => 'required|string',
            'between' => 'nullable|string',
            'reference' => 'nullable|string',
            'latitude' => 'nullable|string',
            'longitude' => 'nullable|string',
        ];
    }
}
