<?php

namespace App\Http\Controllers\Backoffice\Administration\User;

use App\Models\User;
use App\Models\Role;
use App\Http\Requests\Backoffice\Administration\User\UserController\IndexRequest;
use App\Http\Requests\Backoffice\Administration\User\UserController\StoreRequest;
use App\Http\Requests\Backoffice\Administration\User\UserController\UpdateRequest;
use App\Http\Resources\Backoffice\Administration\User\UserController\IndexCollection;
use App\Http\Resources\Backoffice\Administration\User\UserController\ShowResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        $user = auth()->guard('api_user')->user();
        $perPage = $request->per_page ? $request->per_page : 15;
        $orderColumn = $request->order_column ? $request->order_column : 'full_name';
        $sortBy = $request->sort ? $request->sort : 'asc';
        $searchColumn = $request->search_column;
        $search = $request->search;

        $query = \DB::table('users')
            ->select(['id', 'full_name', 'username', 'email', 'phone', 'profile_image', 'role_name', 'status'])
            ->whereNull('deleted_at')
            ->where('role_name', '!=', 'owner');

        if ($search && is_null($searchColumn)) {
            $query->whereRaw("
                id::TEXT LIKE '%{$search}%' OR 
                LOWER(full_name) LIKE LOWER('%{$search}%') OR 
                LOWER(username) LIKE LOWER('%{$search}%') OR 
                LOWER(email) LIKE LOWER('%{$search}%') OR 
                LOWER(phone) LIKE LOWER('%{$search}%') OR 
                LOWER(profile_image) LIKE LOWER('%{$search}%') OR 
                LOWER(role_name) LIKE LOWER('%{$search}%')
            ");
        }
        
        if ($search && $searchColumn ) {
            $query->whereRaw("LOWER({$searchColumn}::TEXT) LIKE LOWER('%{$search}%')");
        }

        $query->orderBy($orderColumn, $sortBy);
        $data = $query->paginate($perPage);

        return $this->responseResourceCollection(new IndexCollection($data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Backoffice\Administration\User\UserController\StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $role = Role::findOrFail($request->role_name);

        try {
            \DB::beginTransaction();
                $user = new User();
                $user->full_name = $request->full_name;
                $user->username = $request->username;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->password = bcrypt($request->password);
                $user->role()->associate($role);
                $user->save();

                $user->permissions()->attach($role->permissions);
            \DB::commit();

            return $this->messageResponse(__('response.Backoffice.Administration.User.UserController.store.success'), 201);
        } catch (\Exception $exception) {
            \DB::rollback();
            return $this->messageResponse(__('response.Backoffice.Administration.User.UserController.store.error'), 500, 'error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $userSesion = auth()->guard('api_user')->user();

        if ($user->role_name === 'owner' && $userSesion->role_name !== 'owner') {
            return $this->messageResponse(__('response.Backoffice.Administration.User.UserController.show.unauthorized'), 401, 'error');
        }
        
        return $this->responseJsonResource(new ShowResource($user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Backoffice\Administration\User\UserController\UpdateRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, User $user)
    {
        $userSesion = auth()->guard('api_user')->user();
        
        if ($user->role_name === 'owner' && $userSesion->role_name !== 'owner') {
            return $this->messageResponse(__('response.Backoffice.Administration.User.UserController.update.unauthorized'), 401, 'error');
        }
        
        try {
            \DB::beginTransaction();
                if ($request->has('role_name') && $request->role_name !== $user->role_name) {
                    $role = Role::where('name', $request->role_name)->firstOrFail();
                    $user->role()->associate($role)->save();
                    $user->permissions()->detach();
                    $user->permissions()->attach($role->permissions);
                }

                if ($request->has('password')) {
                    $request->password = bcrypt($request->password);
                }
                
                $user->update($request->except('password_confirmation'));
            \DB::commit();
            
            return $this->messageResponse(__('response.Backoffice.Administration.User.UserController.update.success'), 200);
        } catch (\Exception $exception) {
            \DB::rollback();
            return $this->messageResponse(__('response.Backoffice.Administration.User.UserController.update.error'), 500, 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            \DB::beginTransaction();
                $user->permissions()->detach();
                $user->delete();
            \DB::commit();
            
            return $this->messageResponse(__('response.Backoffice.Administration.User.UserController.destroy.success'), 200);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return $this->messageResponse(__('response.Backoffice.Administration.User.UserController.destroy.error'), 500, 'error');
        }
    }
}
