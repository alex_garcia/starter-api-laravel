<?php

namespace App\Http\Controllers\Backoffice\Administration\User;

use App\Models\User;
use App\Http\Requests\Backoffice\Administration\User\AddressController\StoreRequest;
use App\Http\Requests\Backoffice\Administration\User\AddressController\UpdateRequest;
use App\Http\Resources\Backoffice\Administration\User\AddressController\ShowResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Backoffice\Administration\User\AddressController\StoreRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, User $user)
    {
        $userSesion = auth()->guard('api_user')->user();
        
        if ($user->role_name === 'owner' && $userSesion->role_name !== 'owner') {
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.store.unauthorized'), 401, 'error');
        }

        if ($user->address) {
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.store.have-address'), 500, 'error');
        }

        try {
            \DB::beginTransaction();
                $user->address()->create($request->all());
            \DB::commit();

            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.store.success'), 201);
        } catch (\Exception $exception) {
            \DB::rollback();
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.store.error'), 500, 'error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $userSesion = auth()->guard('api_user')->user();
        
        if ($user->role_name === 'owner' && $userSesion->role_name !== 'owner') {
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.show.unauthorized'), 401, 'error');
        }

        if (! $user->address) {
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.show.not-have-address'), 404, 'error');
        }

        return $this->responseJsonResource(new ShowResource($user->address));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Backoffice\Administration\User\AddressController\UpdateRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, User $user)
    {
        $userSesion = auth()->guard('api_user')->user();
        
        if ($user->role_name === 'owner' && $userSesion->role_name !== 'owner') {
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.update.unauthorized'), 401, 'error');
        }

        if (! $user->address) {
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.update.not-have-address'), 404, 'error');
        }

        try {
            \DB::beginTransaction();
                $user->address()->update($request->all());
            \DB::commit();
            
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.update.success'), 200);
        } catch (\Exception $exception) {
            \DB::rollback();
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.update.error'), 500, 'error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $userSesion = auth()->guard('api_user')->user();
        
        if ($user->role_name === 'owner' && $userSesion->role_name !== 'owner') {
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.destroy.unauthorized'), 401, 'error');
        }

        if (! $user->address) {
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.destroy.not-have-address'), 404, 'error');
        }

        try {
            \DB::beginTransaction();
                $user->address->delete();
            \DB::commit();
            
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.destroy.success'), 200);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return $this->messageResponse(__('response.Backoffice.Administration.User.AddressController.destroy.error'), 500, 'error');
        }
    }
}
