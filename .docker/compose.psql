version: "3.8"

services:

###########################################################
#### IMAGE APPLICATION
###########################################################

    application:
        build:
            args:
                VERSION: ${APPLICATION_VERSION}
                USER: ${APPLICATION_USER}
                DB_TYPE: ${ENV_TYPE_DATABASE}
                UID: ${APPLICATION_PASSWORD}
                PHPREDIS_VERSION: ${APPLICATION_PHPREDIS_VERSION}
                TIME_ZONE: ${ENV_TIME_ZONE}
            context: ${APPLICATION_CONTEXT}
            dockerfile: ${APPLICATION_DOCKERFILE}
        image: ${APPLICATION_IMAGE_NAME}
        container_name: ${APPLICATION_CONTAINER_NAME}
        restart: ${APPLICATION_RESTART}
        working_dir: ${APPLICATION_WORK_PATH}
        environment:
            ENV_HOSTNAME: ${ENV_HOSTNAME}
            ENV_PROTOCOL: ${ENV_PROTOCOL}
            APP_NAME: ${ENV_APP_NAME}
            APP_TIMEZONE: ${ENV_TIME_ZONE}
            APP_LOCALE: ${ENV_LANGUAGE}
            DB_TYPE: ${ENV_TYPE_DATABASE}
            DB_HOST: ${POSTGRES_BUILDER_NAME}
            DB_PORT: ${POSTGRES_PORT}
            DB_DATABASE: ${POSTGRES_DATABASE}
            DB_USERNAME: ${POSTGRES_USER}
            DB_PASSWORD: ${POSTGRES_PASSWORD}
            CACHE_DRIVER: ${ENV_CACHE_DRIVER}
            SESSION_DRIVER: ${ENV_SESSION_DRIVER}
            REDIS_HOST: ${REDIS_BUILDER_NAME}
            REDIS_PASSWORD: ${REDIS_PASSWORD}
            REDIS_PORT: ${REDIS_PORT}
        volumes:
            - ${APPLICATION_CODE_PATH}:${APPLICATION_WORK_PATH}
        networks:
            - development_app

###########################################################
#### IMAGE REDIS
###########################################################

    redis:
        image: redis:${REDIS_VERSION}
        container_name: ${REDIS_CONTAINER_NAME}
        restart: ${REDIS_RESTART}
        command: redis-server --requirepass ${REDIS_PASSWORD}
        volumes:
            - redis_volume:/data
        ports:
            - ${REDIS_PORT}:${REDIS_CONTAINER_PORT}
        networks:
            - development_app

###########################################################
#### IMAGE NGINX
###########################################################

    webserver:
        image: nginx:${NGINX_VERSION}
        container_name: ${NGINX_CONTAINER_NAME}
        restart: unless-stopped
        ports:
            - ${NGINX_PORT}:${NGINX_CONTAINER_PORT}
        volumes:
            - ${NGINX_CODE_PATH}:${NGINX_WORK_PATH}
            - ${NGINX_CONFIG_PATH}:/etc/nginx/conf.d
        networks:
            - development_app

###########################################################
#### IMAGE DATABASE PostgreSQL
###########################################################

    database: 
        image: postgres:${POSTGRES_VERSION}
        container_name: ${POSTGRES_CONTAINER_NAME}
        restart: ${POSTGRES_RESTART}
        ports:
            - ${POSTGRES_PORT}:${POSTGRES_CONTAINER_PORT}
        volumes:
            - data_db_volume:/var/lib/postgresql/data
        environment:
            POSTGRES_DB: ${POSTGRES_DATABASE}
            POSTGRES_USER: ${POSTGRES_USER}
            POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}

        networks:
            - development_app

###########################################################
#### IMAGE PHPMYADMIN
###########################################################

    pgadmin:
        image: dpage/pgadmin4:${PGADMIN_VERSION}
        container_name: ${PGADMIN_CONTAINER_NAME}
        restart: ${PGADMIN_RESTART}
        ports:
            - ${PGADMIN_PORT}:${PGADMIN_CONTAINER_PORT}
        environment:
            PGADMIN_DEFAULT_EMAIL: ${PGADMIN_DEFAULT_EMAIL}
            PGADMIN_DEFAULT_PASSWORD: ${PGADMIN_DEFAULT_PASSWORD}
        networks:
            - development_app

###########################################################
#### VOLUMES SETTINGS
###########################################################

volumes:
    data_db_volume:
        driver: local
    redis_volume:
        driver: local
              
###########################################################
#### NETWORK SETTING
###########################################################

networks:
    development_app:
        driver: bridge