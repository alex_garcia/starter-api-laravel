<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Backoffice\Account\Auth\AuthController;
use App\Http\Controllers\Backoffice\Administration\User\UserController;
use App\Http\Controllers\Backoffice\Administration\User\AddressController;

// PREFIX V1
Route::prefix('v1')->name('backoffice.')->group(function () {

    // POST api/v1/account/login
    Route::post('/account/login', [AuthController::class, 'login'])->name('account.auth.auth.login');

    Route::middleware(['auth:api_user'])->group(function () {

        // PREFIX ACCOUNT & GROUP ACCOUNT
        Route::prefix('account')->name('account.')->group(function () {

            // GROUP AUTH
            Route::name('auth.')->group(function () {
                
                // GROUP AUTH CONTROLLER
                Route::name('auth.')->group(function () {

                    // PUT api/v1/account/logout
                    Route::put('logout', [AuthController::class, 'logout'])->name('logout');

                    // PUT api/v1/account/refresh
                    Route::put('refresh', [AuthController::class, 'refresh'])->name('refresh');

                    // GET api/v1/account/me
                    Route::get('me', [AuthController::class, 'me'])->name('me');
                });
            });
        });

        // GROUP ADMINISTRATION
        Route::name('administration.')->group(function () {

            // GROUP USER
            Route::name('user.')->group(function () {

                // GROUP ADDRESS CONTROLLER
                Route::name('address.')->group(function () {

                    // POST api/v1/user/:user/address
                    Route::post('/user/{user}/address', [AddressController::class, 'store'])->name('store')->middleware('can:user-address-store');

                    // GET api/v1/user/:user/address
                    Route::get('/user/{user}/address', [AddressController::class, 'show'])->name('show')->middleware('can:user-address-show');

                    // PUT api/v1/user/:user/address
                    Route::put('/user/{user}/address', [AddressController::class, 'update'])->name('update')->middleware('can:user-address-update');

                    // DELETE api/v1/user/:user/address
                    Route::delete('/user/{user}/address', [AddressController::class, 'destroy'])->name('destroy')->middleware('can:user-address-destroy');
                });

                // GROUP USER CONTROLLER
                Route::name('user.')->group(function () {
                    
                    // GET api/v1/user
                    Route::get('/user', [UserController::class, 'index'])->name('index')->middleware('can:user-index');

                    // POST api/v1/user
                    Route::post('/user', [UserController::class, 'store'])->name('store')->middleware('can:user-store');

                    // GET api/v1/user/:user
                    Route::get('/user/{user}', [UserController::class, 'show'])->name('show')->middleware('can:user-show');

                    // PUT api/v1/user/:user
                    Route::put('/user/{user}', [UserController::class, 'update'])->name('update')->middleware('can:user-update');

                    // DELETE api/v1/user/:user
                    Route::delete('/user/{user}', [UserController::class, 'destroy'])->name('destroy')->middleware('can:user-destroy');
                });
            });
        });
    });
});
